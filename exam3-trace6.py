decimal_number = 0
position = 7
placeholder = 128
binary_num = input('Enter the 8-bit binary number: ')
i = 0
while position != 1:
    bit = int(binary_num[i])
    if bit == 1:
        value = bit * placeholder
        decimal_number += value
    position -= 1
    placeholder /= 2
    i += 1

print(decimal_number)
