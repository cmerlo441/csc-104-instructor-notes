#! /usr/bin/env python3


def my_function(x, y):
    return (2 * x) - y


print('The first value is', my_function(3, 2))
print('The second value is', my_function(2, 3))
print('The third value is', my_function(6, 13))
print('The fourth value is', my_function(13, 6))
