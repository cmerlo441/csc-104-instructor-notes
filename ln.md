## Copyright

These instructor notes are © 2019 Christopher R. Merlo.

This work is licensed under the Creative Commons Attribution-ShareAlike
4.0 International License. To view a copy of this license, visit
<http://creativecommons.org/licenses/by-sa/4.0/> or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

![image](cc.logo.eps) ![image](by.eps) ![image](sa.eps)

## About These Notes

Dear Instructor,

These notes are meant to serve as your companion to the
**<span style="color: mLightBrown">CSC 104 Lecture Notes</span>**, which
are designed to be read by the students before each lecture. Whereas
that document provides some introductory material about each day’s
topics, this document will describe the activities you should lead in
class.

### Color Scheme

Notice that Day titles and page headings in this document are typeset in
**<span style="color: mLightGreen">green</span>** to help you notice at
a glance which document you’re looking at. These items in the lecture
notes are in **<span style="color: mLightBrown">orange</span>**.

### A Typical Day in Class

You should start each day by displaying the Introduction slide, to
remind students – who should have read the lecture notes before class –
of what the current day’s topic is. The slides are designed to move you
through the material in the right order, and to keep students apprised
of what the current discussion is about.

Note that the slides are just a PDF document. Display the PDF in
full-screen mode, and you will be able to use the up and down arrow keys
to navigate between slides.

#### Displaying the Slides on a Mac

If you expect to use a Mac to display these slides to the class, allow
me to introduce you to
[Présentation.app](http://iihm.imag.fr/blanch/software/osx-presentation/).
The built-in Preview application, and other applications I’ve used,
present a blurry, low-resolution version of the PDF, and then take about
a second to render it. This application, however, was designed for
slides created the way the Lecture Slides were created (using
[Beamer](https://en.wikipedia.org/wiki/Beamer_\(LaTeX\))), and it has a
bunch of nice presenter features also. See [this discussion on
StackExchange](https://tex.stackexchange.com/questions/431423/os-x-blurry-beamer-presentations-on-sierra-high-sierra)
for more information.

Note that Présentation requires Python’s Objective-C module; see [this
StackOverflow
answer](https://stackoverflow.com/questions/4678186/no-module-named-objc)
if you get the error `ModuleNotFoundError: No module named ’objc’` when
trying to run Présentation.

### Homework

Wherever possible, I have always graded homework in a way that
encourages students to submit something, even if it’s wrong. A
thoughtful wrong answer is *always* better than no answer. I have always
given a minimum grade of 90 to any thoughtful, complete homework
submission (where possible), and 100 for a correct answer. Programming
should not be adversarial, and neither should this course.

#### Collecting Homework

Many of us use a course management software package such as OCSW or
Blackboard. I do, and I expect students to type their answers to
homework and submit them electronically. Because of this, you will
notice that most homework handouts are ***not*** formatted in a way that
makes it easy to write in answers.

I choose to have students type and submit plain-text files when
answering homework. Programming is hard enough; having to work out what
a word processor is trying to do while typing answers to technical
questions is too high a cognitive load for *me*. I can only imagine the
struggle a freshman or sophomore would have.

### Updates

This document, as well as the Lecture Notes and the Lecture Slides, will
be available from <http://www.matcmp.ncc.edu/csc104>. I will also post
various handouts from the end of this document there as individual
documents, so that you can easily share them with your students. You
will need to log in as an instructor to access these notes and the
slides, but students can download their notes from there.

You should subscribe to our Slack server,
<https://pythonforcsc104.slack.com>, so that you know right away when
any updates to this document are posted.

### Contact Me

Be sure to ask me for help or clarification any time\! While you can
email me at `cmerlo@ncc.edu`, you will probably get a more timely answer
on Slack.

Enjoy the course\!  
– Chris

\[2\]\[\]<span> float=htb, capture=hbox, blend before title=colon hang,
every float=, sharp corners=downhill, colback=white,
colframe=mDarkGreen, coltitle=mLightBrown, </span>

# Problem Solving

## Course Introduction

### Interview Questions

Have students read the article on interview questions, and discuss for a
while. I like to point out that as a programmer, people think that my
brain is somehow different from theirs, but really I’ve just trained it
to be able to classify a problem and think about what kind of solutions
it might have. Reinforce the idea that this course will focus on
problem-solving skills, and how to approach a novel problem.

### Logo Exercise

You should have a pack of index cards with logos on them. This exercise
is intended to introduce students to the difficulties of writing and
following directions, and also to the frustrations that might be found
in dealing with a restricted language.

Explain to the students that once a solution to a problem has been
developed, we programmers must give precise, detailed instructions to
the computer so that it executes our solution the way we intend.

#### Step 1: Describing the Logo

Distribute the index cards, one to each student, along with the handout
on Page , which contains these instructions:

Choose a symbol from your instructor. **Do not** show the symbol to
anyone else. In the space below, write clear instructions on how to draw
it, but **do not** state what the symbol is. Write your instructions as
though a robot is going to follow them, and the robot only knows about
basic shapes (for example: circles, ovals, rectangles, squares,
triangles), lines, thickness of lines, curves in lines, angles,
pen/pencil movements, directions, measurements, etc. The robot does not
know the letters of the alphabet, and will not understand the names of
any companies. When you’re done, give this paper to your instructor.

Believe it or not, this is the hard part for many students. They are not
used to restricting their language in this way. You may want to remind
them every couple of minutes not to use letters of the alphabet or names
of corporations.

Make sure students write the logo number in the appropriate place on the
paper.

Also, make sure that students **do not draw** the logo they’re writing
about.

Give students about ten minutes to finish this activity.

#### Step 2: Drawing the Logo

I have personally found this step to be more fun when I’ve chosen
***not*** to tell the students that this step is coming. Collect the
papers, let the students keep the index cards for now (face down), and
randomly redistribute the papers, along with the handout from Page ,
which says:

Draw the symbol described in the written instructions that were given to
you.

Students will be amused, confused, and frustrated. Reassure them that
this is a learning exercise, not a grading exercise, and that it’s ok to
not be right.

#### Step 3: Wrap Up

Ask students to volunteer to show off their logo drawing skills. I
usually ask “Who’s sure they got this one right?” and have volunteers
show their work to the class, as the student who has the matching index
card shows his or her logo to the class. This will get two or three
students to engage. Then I ask “Now, who has no idea what they were
supposed to be drawing?” and a lot more hands will go up.

Use this opportunity to talk about the difficulties that are inherent in
writing instructions and in reading instructions. Ask students if they
have any intuition regarding which one is harder to do. Assure them that
as the semester goes on, they will get better at both.

## Talking Like a Programmer

### Interview Question: The Rope Bridge

The first slide today after the introduction presents students with the
first “interview question” that they will grapple with this semester.
Many people will assume that sending 10 across the bridge with 1 will
somehow minimize 10’s effect on the overall time. There is no way to
team those two people up and beat 17 minutes. After I let students think
about this for a bit, I interrupt and say:

You may have assumed something about the solution to this question, that
is blocking you from arriving at the solution. Question your
assumptions, and be prepared to throw them out.

|  **Here**   |                         **Crossing**                          |  **There**  | **Time Spent** | **Total Time** |
| :---------: | :-----------------------------------------------------------: | :---------: | :------------: | :------------: |
| 1, 2, 5, 10 |                                                               |             |       \-       |       0        |
|    5, 10    |                     1, 2 \(\rightarrow\)                      |             |       2        |                |
|    5, 10    |                                                               |    1, 2     |                |       2        |
|    5, 10    |                       \(\leftarrow\) 1                        |      2      |       1        |                |
|  1, 5, 10   |                                                               |      2      |                |       3        |
|      1      | <span style="color: mLightBrown">5, 10 \(\rightarrow\)</span> |      2      |       10       |                |
|      1      |                                                               |  2, 5, 10   |                |       13       |
|      1      |                       \(\leftarrow\) 2                        |    5, 10    |       2        |                |
|    1, 2     |                                                               |    5, 10    |                |       15       |
|             |                     1, 2 \(\rightarrow\)                      |    5, 10    |       2        |                |
|             |                                                               | 1, 2, 5, 10 |                |       17       |

Answer to the Rope Bridge Exercise

The solution requires pairing 10 up not with 1, but with 5. We want 10
to absorb as much of the cost of the crossing as possible. Since 10 has
to cross anyway, pairing 10 with 5 hides the effect of 5’s lack of
speed.

### Vocabulary

Have students answer the questions on the slides. Reading verbatim out
of the lecture notes doesn’t prove understanding, so discuss as
necessary.

### Three Fundamental Components of a Program

Students probably have no context for understanding this. I always say
at the time that they’ll have to know about these things later, so
remember when we talked about it.

### Writing Directions

When I teach this course on the first floor, I tend to have students
write directions to B 225. When I’m on the second floor, I tend to have
students write directions to the snack bar. This gets students to have
to use the word “step” in two different ways, which helps put students
in the mindset that there’s frequently an extra layer of complexity that
they may not have considered.

After they come back, I have the students share their results with the
class, while I act them out and point out their flaws. For instance,
when a student says “Turn left,” I turn to the left and keep spinning
until they see how little information they provided. This course should
be fun.

### Homework

Encourage students to submit an answer, even if they don’t think it’s
right. A wrong answer, arrived at through logical problem solving, is
always better than no answer.

## Matrix Logic

### What is Matrix Logic?

Discuss homework problem, and how a bit of extra information – namely,
who’s married to whom – was necessary to complete the puzzle.

### Example Matrix Problem

I included this example in the lecture notes for students to read before
class, so that they have some idea of what’s coming during lecture. I do
***not*** anticipate discussing it in class for more than about a minute
or two. Today is a hands-on day, and students have more to do than stuff
they should have done before class.

### Matrix Logic Exercises and Homework

Hand out the barbecue problem on Page , and give students about 20
minutes or so to try to solve it. After that, expect to spend about
10-15 minutes solving it with them (I recommend beaming it on the
whiteboard and filling in the matrix).

Then hand out the Football Problem handouts from Pages and . I let them
work in groups on this one, even though I assign it for homework.

### After Class

Students may want their own copies of these handouts. I’ve uploaded all
of them to <http://www.matcmp.ncc.edu/~cmerlo/csc104/handouts/>. You can
download them from there and upload them to OCSW or whatever CMS you’re
using, or email them, or whatever.

## Systematic Lists

### Interview Question

I often explain how to solve this question with a tree of outcomes.
Three players will result in one winner:

Then I ask where did those three winners come from? They came from
earlier matches:

So, how far back do we have to go to get 81 players in a level? Well, if
there are three players on the first level, and then nine on the second,
see if students can work out that there would be 27 on the third level,
and 81 on the fourth.

Finally, remember that the question asks *how many games* it takes to
find the winner. Since each game has a winner, we need only count the
interior nodes of the tree – there are 27 on the third level, 9 on the
second, 3 on the first, and finally one winner on level zero – to come
up with 40 matches.

### Homework Question: The Football Problem

Remember: this was probably a difficult question for *you*, so be
patient with the students that didn’t get it. Also, reassure the
students that nothing this challenging will appear on the exam – the
most complicated question on the exam will be like the Barbecue Problem
from Day [3](#day:matrix).

### Systematic Lists

The one point that I often worry about whether I’ve made clear enough to
students when teaching systematic lists is this: The result must have
been obtained *systematically*, by following a *system*. My system
always involves non-decreasing values in the left-most column, but your
mileage may vary. The right answers in the wrong order is an incorrect
solution, and you can always explain this by pointing out the two (or
more) different systems that were used to compose different parts of the
list.

## Binary Numbers

### What Are Binary Numbers?

Hopefully students have read the lecture notes. Spend a little time, but
not a lot.

### Why Do We Need to Know About Binary Numbers?

This is a question that probably needs some discussion. I’ve always
explained that at the most basic level, computers operate based upon the
presence or absence of an electrical charge. The computer’s physical
memory consists of millions of transistors, each of which can be in some
binary state, whether “off” or “on,” or “open” or “closed,” and these
two states can be represented by the digits 0 and 1 in the binary
numbering system. (When data is transferred to a magnetic or optical
disk, the "ons" and "offs" of the transistors are converted to patches
of up-or-down magnetization on the magnetic medium or light-or-dark
spots on the optical medium.)

### Binary Game \#1

Students will have eight cards in front of them that, if they’ve
followed your instructions, look like this:

  128 64 32 16 8 4 2 1  

Ask the students what they notice about the numbers on the cards. Let
them try to find all of these conditions, and help them see the ones
they don’t mention:

  - Card to the right is half of the one to the left / card to the left
    is double the one to the right

  - The sum of the last \(n\) cards is equal to one less than the
    \(n+1\)th card

  - Each value is a power of 2, specifically \(2^0\) through \(2^7\)

Now start playing the “turn cards over” game. Notice that the slide
contains sums that require two cards, three cards, etc. Also notice the
question “Can you make 5 a different way?” You can refer back to this
later, after more examples, to help illustrate that decimal-to-binary
conversions are unique.

### Homework

A handout is available on Page for homework.

## Introduction to Conditionals

### Interview Question

Give students a few minutes on this interview question:

A guard has to take a wolf, a sheep, and a cabbage across a stream in a
boat. The boat has room for the guard and one of the other things. The
wolf will eat the sheep if left alone with it, and the sheep will eat
the cabbage if left alone with it. How can the guard get all these items
across the river?

The important characteristic of the answer is that the sheep must not be
left alone, so the guard takes the sheep across, goes back alone, takes
either the wolf or the cabbage across, returns with the sheep, takes the
wolf/cabbage (whatever he didn’t take last time), leaves the wolf and
the cabbage on the other side, and then goes back for the sheep.

### What We Have Covered So Far

So far, we have talked about these things together:

  - Sequential instructions

  - Limited vocabulary

  - Types of errors – syntax vs. semantic

  - How the computer stores information

We also mentioned that there are three kinds of statements, or three
fundamental components, in any software: sequence, selection, and
iteration. We’re going to start talking about selection, which is how
the computer can make decisions based upon certain circumstances or
conditions.

### Introduction to Conditionals

Discuss these questions with the students. Don’t go on until you get
satisfactory answers:

  - How do you determine if you need an umbrella?

  - How do you determine if you need a jacket?

  - How do you determine if you need to go to CSC 104?

Lead students to the idea that the answer to each of these questions is
based on a *condition*.

#### Relational Operators

Note that Python’s relational operators are the same as those found in
Java:

 `<` `<=` `>` `>=` `==` `!=`  

### In-Class Exercises

Use the slides to lead students through the exercises. Handouts to be
used:

  - Simple Conditionals on page

  - If-Else Conditionals on page

For each scenario, help students see what the ***present state*** of the
object under discussion is, how we choose a ***possible value*** for
that object, and what possible ***outcomes*** each condition has. If we
use this vocabulary consistently during class, on slides, and in
handouts, students will hopefully have an easier time talking and
writing about these ideas.

### Homework

Assign the Day [6](#day:cond1) Homework found on page , but understand
that half of it relies on Day [7](#day:cond2) material, so don’t make it
due until after Day [7](#day:cond2).

## Nested Conditionals and Complex Conditionals

### A Note About Grading

Many of you reading this have taught this course before – some of you
*created* this course – and you don’t need me to tell you how to grade
homework and exams. But I have to ask you for a favor.

I have taught a lot of different programming courses, and I have seen
students struggle with writing conditional statements. I believe that
the struggle is due, at least in part, to not understanding the goal. In
the lecture notes, I tried to stress the goal of writing conditionals as
getting to the right outcome as efficiently as possible – without extra
if or else clauses, and without unneeded Boolean operators. If they
struggle with these things in this class, they aren’t going to fare much
better in CSC 120 or ITE 154.

We all know that we can talk and draw pictures all we want, and students
aren’t going to notice until we start docking points. This is why I have
always told students – and why I wrote in the Lecture Notes – that
grades will be determined in part by the *efficiency* of their
statements. I will take points off for things like

``` python
if temperature > 70:
    do stuff

elif temperature <= 70 and temperature > 60:
    do different stuff
```

because it isn’t just too much code. It’s an indication that students
are not evaluating the situation carefully. Thank you for enduring this
rant.

### Nested Conditionals

Walk the students through the slides – there aren’t many because I tried
to present everything in the Lecture Notes – and then the exercises in
the handout on page .

Watch your time, because you still have to cover complex conditionals
today.

Note that there are multiple acceptable answers for questions like this,
and which one a student develops is based on how they initially think
about the problem. Considering the first condition in the handout, an
answer like:

``` python
if numberOfBooks = 1:
    1 point
elif numberOfBooks < 5:
    3 points
else:
    10 points
```

is equally acceptable to an answer like:

``` python
if numberOfBooks >= 5:
    10 points
elif numberOfBooks = 1:
    1 point
else:
    3 points
```

Also, note that however much attention you pay to punctuation now –
proper use of relational operators, colons at the end of clauses, etc. –
will pay off for you and the students when you start actually coding
four class meetings from now.

### Complex Conditionals

Same idea. Like the nested conditionals, it is possible to develop a
“correct” solution that uses too many conditions, so help students see
where they can cut stuff out. Have them do the handout on page .

### Homework

Remind students that they should now be able to complete the homework
assignment you gave out at the end of Day [6](#day:cond1), and that
answers should be submitted before the start of the next class.

## Debugging Conditionals

### DVR Example

The slides introduce an and/or problem. Some students will see the error
right away, but others will need some extra time, and maybe even some
extra explanation. Ask questions like “Will my DVR record on Tuesday at
2:00 pm? Friday at 9:30 am? How about Wednesday at 12:00 pm? Sunday at
5:30 am?” and lead them to the bug.

### Handout

Students should spend the rest of the class working on the handout on
Page . Don’t just give it to them and sit back. Walk around and throw
questions out. For the first exercise – where the first five cases are
correctly handled by the conditional statement, and only the last one
illustrates the bug – as students start to get close to done, I throw
out a comment like “Man, do we have to check all these cases?” I also
tend to discuss the first exercise as a group when most students have
completed it, before introducing the second one.

## Exam 1 Review

### Review Sheet

Handout the review sheet, and allow students to work. I usually break in
and start a group discussion with about 20 or 25 minutes left in class
if there haven’t been any group discussions before then.

### zyBooks

Now is probably a good time to remind students that on Day
[1](#day:pythonintro) there will be zyBooks homework due. Instructions
for how students gain access are available to you when you log in to
zyBooks. Click on the “Welcome” tab at the bottom of the right-side pane
(the top of this right-side pane has a green background and says “CSC
104”), and then, under the heading that says “Instructions for your
students”, click “View instructions.”

## Exam \#1

### The Exam

Administer Exam \#1.

### Homework

#### zyBooks

Expect to cover at least Sections 1.1 through 1.3 of the online textbook
on Day 11. Assign the Participation Activities from 1.1 – 1.4 to be done
before the next class starts. Some students may have waited until now to
pay for the textbook, and those students must now purchase access.

These Participation Activities will introduce students to the Python
programming language and some of the basic things they will be doing
throughout the rest of the course. You can do these activities too\!

#### Python and Idle

For those students who want to use their own computers in class, tell
them to have Python installed before the next class begins. There is an
appendix in the Lecture Notes that describes how to get set up in
Windows, Mac, or Linux.

# Programming in Python

## Introduction to Python

### Exam 1

Hand Exam \#1 back to the students. Take 20 minutes or so going over it.

### IDLE

I recommend that you and the students write Python code, at least
initially, using the IDLE environment. It comes packaged with Python, so
once Python is installed, there is nothing else to install. The Lecture
Notes has an appendix devoted to downloading and running IDLE for
Windows, Mac, and Linux.

You will probably want to spend a couple of minutes getting IDLE running
on your computer, on the overhead, so students can see what you’re
doing. Some students may have already installed it on their laptops;
others will try to do what you’re doing on the school’s computers. In
Windows, it should be as easy as typing “IDLE” into the search box at
the bottom left.

I expect to be switching between the slides and IDLE quite a bit these
next couple of class sessions. Alt-Tab is your friend.

Once you have it working, get it to print “Hello world\!” like in Figure
[\[fig:hw\]](#fig:hw) on Page .

<span>Hello World in IDLE on Windows</span>
<span id="fig:hw" label="fig:hw">\[fig:hw\]</span>
![image](IDLEwin-hw.png)

### Interactive Programming

We’ll get to saving and loading files another day. Use today to get
students used to IDLE, and the syntax of elementary Python. Hopefully
the immediacy of results in interactive mode will spur them on to trying
things.

We will essentially be covering Sections 1.1 through 1.3 of the textbook
today. Students should have completed the Participation Activities from
these sections before coming to class.

#### Print the Student’s Name

Show them how to print their own name as a string literal (both single
and double quotes are ok). Then show them how to store their name in a
string variable and display it, like `print("Hello " + name)`.

#### Print the Student’s Name Interactively

Follow the slides to work up to `print('Hello', name, '!')` and then
eventually:

``` python
name = input( "What's your name? " )
print( 'Hello', name, '!' )
```

##### Two Kinds of String Concatenation

You can use `+` to concatenate strings just like in Java. Python also
allows use of the comma operator `,` to append with a space, like
`print('Hello', name)`. The slides work the students through the
differences, and then help them get to:

``` python
name = input( "What's your name? " )
print( 'Hello', name + '!' )  # now with a +
```

#### Do a Little Math

Things like `print( 3 * 2 )` and `print( 3 ** 2 )` are neat at first.

Students might ask how to find a square root, and it’s tricky. The
`sqrt()` function doesn’t work unless you import the math library, and
let’s not get into all that just yet. But you can raise a number to a
decimal exponent, like `print( 9 ** ( 0.5 ) )`.

In Python 2 (*not* what we’re using here), 1/2 was equal to 0, like in
Java, where integer division always generates an integer. In Python 3
(what we’re using here), 1/2 will result in 0.5. As a matter of fact,
division with the `/` operator *always* returns a double, so `(print( 4
/ 2 )` will display 2.0, not 2. If students do `print( 9 ** ( 1 / 2 ) )`
and get 1, they’re likely using Python 2, and they should stop doing
that ASAP and get Python 3 installed.  
The Java-style division, what they call *floor division* in Python, is
available with the `//` operator, so `print( 1 // 2 )` will display 0.
Note that floor division with a double returns a double, so `print( 1.0
// 2 )` displays 0.0.  
This all comes up in Chapter 2, so don’t stress if you don’t cover it
today.

#### Data Types

Python has data types, but they are often hidden from us. We don’t
generally need to know or use type names in Python code, the way you
would see `int` or `String` all over your Java code.

This all gets explained better in Chapter 2 of the zyBooks, and so we
don’t need to say much now, but it comes up when we want to do any
arithmetic.

We can just store a number in variable like `myVariable = 5`, and we can
even *change the type of a variable* by assigning it a new value:

``` python
myVariable = 5
myVariable = 'Python is cool'  # yes, that's legal
```

The variable’s type has changed, but we didn’t have to tell it to
change. Largely this all Just Works\(^{\mathrm{TM}}\)... until we try to
do input. The `input()` function retrieves a string from standard input.
We must use the `int()` function to convert a string to an integer when
necessary. See the slides.

### Homework

Assign the Challenge Activities from Chapter 1, Sections 1 – 3 of
zyBooks, and assign the Participation Activities from Section 4 and
onward of Chapter 1. Remind students that completion of the
Participation Activities helps set up the next day’s lecture.

## Python Errors and Introduction to zyLabs

### Python Errors

Students should have read Section 1.4 of the book and completed the
Participation Activities, and so they should be aware of the three kinds
of errors – syntax errors, runtime errors, and semantic or logic errors.
Run through the slides with them, but this shouldn’t take more than
about 15 minutes. You will want the rest of the time to walk through the
zyLabs stuff.

I strongly recommend that you take this opportunity to remind those
students who haven’t completed the preparation for this lecture that
they are now behind, and you will not be spending their classmates’ time
catching them up. How gently you provide this reminder is up to you, of
course, but be firm, because the density of information is going to ramp
up, and students need to understand their responsibility to prepare for
class.

### zyLabs

The online textbook from zyBooks includes interactive programming
exercises as part of each chapter, that students can work on during
class and receive immediate grades. We will use these zyLabs as homework
assignments during the next few weeks, but we can also augment them with
other assignments as well.

#### Before You Begin

##### Create an Assignment

Before going to class today, I recommend you create an assignment in
zyBooks for these zyLabs, like you have done for the Participation
Activites and Challenge Activites. You can “Save and Hide” the
assignment when you create it, so that it’s all set up for when class
begins, and then all you have to do is click on the Assignments tab at
the bottom right, and then the “Hidden” button to the left of “+
Assignment,” and then reveal the assignment. It’s my plan to make this
assignment due at the start of the next class meeting.

Add these labs to the assignment:

  - Basics

  - Interleaved input/output

  - Formatted output: Hello World\!

  - Formatted output: No Parking Sign

  - Input: Welcome message

  - Input: Mad Lib

Students should complete several of these during class, and should be
able to get the rest done before the start of the next class. Remind
students that help is available in the Computer Learning Center as well
as with you during office hours.

##### Develop Mode and Submit Mode

Each zyLab has two modes that the student can be in. In develop mode,
students can code and debug as much as they like without us noticing.
When they think they are done, and are ready to earn a grade for their
work, they can switch to submit mode. In submit mode, the zyLab runs
their code with prepared test cases, looking for predetermined output,
and reports how closely the student’s output conforms to the expected
output for each test case.

In the early going, I would not personally choose to be very nitpicky
regarding scoring these assignments, but in the later part of the
semester, I might announce that any grading runs after the second or
third will cost points.

We can also set a maximum amount of submissions per lab, and we can set
a minimum waiting period between submissions. Again, I wouldn’t do this
now, but maybe near the end of the semester. We can also protect a lab
with a password, if you only want, say, the students in class that day
to access the lab. I would never choose that option, but it’s there.

#### Basics

The first zyLab is called “Basics”, and it’s meant to get students used
to the system. The programming assignment is very simple – ask the user
to input an integer, and display its square. Some code is provided, and
the student is asked to submit it, see how it’s wrong, and then fix it
and resubmit it.

##### Whitespace

Many of the zyLabs will consider the amount and type of whitespace when
determining a match in a program’s output. For some programs this seems
like an appropriate choice, but you may decide that for other examples
the whitespace in the output doesn’t matter as much. In my early
experiences using zyLabs in CS 1, we encountered both situations. There
is a way to configure whitespace checking, and I will **COME BACK AND
EDIT THIS** once I find it again.

#### Interleaved Input/Output

The next lab provides valuable experience in helping students to
differentiate between a program’s expected output in an example run, and
what the user of the program would have entered. It also helps students
understand that some newlines in a sample run are created by the
program, and others are created by the user pressing the Enter key after
input is typed in.

This lab, like the previous one, has students go through the
submit/edit/resubmit cycle again, reinforcing how the tools work. This
is the last of the “training” labs before the “real work” starts (in
quotes because the first “real” lab is a Hello World lab).

These two training labs shouldn’t take more than 15 minutes
collectively. After that, move students on to Hello World and the labs
that follow.

#### Formatted output: Hello World\!

This lab should not take students more than two minutes. It’s literally
“Hello World\!” Remind students that the output must be an exact match
to “Hello World\!”, including capitalization, punctuation, and spacing.
Be prepared to move on to introducing the next one, because the majority
of students will be ready to. A couple of students will need some help,
but hopefully just syntax help.

#### Formatted output: No parking sign

This lab is similar to the Hello World lab, but there are two lines of
output and weirder spacing issues. Even some students who breezed
through everything else may struggle with this. Use the opportunity to
talk about zyLabs’ focus on output formatting.

#### Input: Welcome message

Alert students to the existence of the sample output above the editing
pane. Some students will not understand right away that rather than just
making the program output the sample every time, they must instead
incorporate user input into the output.

#### Input: Mad Lib

Some students aren’t going to understand right away that their code
should be inserted ***between*** the first line (the triple-quote
comment) and the fourth line, which outputs the user’s input and some
other text. You may have to hold their hand through that.

### Homework

Students should complete these zyLabs by next time. Also, assign the
Challenge Activites from Chapter 1, Sections 4 and later, and the
Participation Activites from Chapter 2 Sections 1-7.

## Variables and Expressions

Here’s where we start to see what makes Python different from other
programming languages. If you have worked with another “weakly-typed”
language like JavaScript this won’t be so weird, but if all your
programming experience is in Java and C, you’re in for some new ideas.

### Review

I added a slide to give you time to talk about the labs and the
activities that students have had to do up to now. You’ve had a week of
Python together, and this is a good time to reflect on what makes sense,
what doesn’t make sense, what might need further elucidation, etc.

### Variables in Python

Variables in Python don’t get declared. There’s no `var x` or `int x`
like in other languages. Typing `x = 5` creates `x` if necessary before
storing 5 in it.

#### Identifiers

You already know from other languages what there is to know about
identifiers. It’s interesting to note that the [official Python Style
Guidelines (PEP 8)](#https://www.python.org/dev/peps/pep-0008/) dictate
that it is correct to `use_underscores` for variable names, and to
`useCamelCase` for class names.

There’s a slide to help students reinforce what they’ve read about
acceptable variable identifiers.

### Objects

The word “object” has a different meaning in Python than it does in Java
or C++. Even primitive data are stored in objects.

As explained in zyBooks, an object is just a container for data.
Everything from what we know as primitive values up to fancy data
collections and instances of classes we design are stored in objects.

In Python, an object has these three characteristics:

  - Value

  - Type

  - Identity

When we create or change the value of a variable, the type doesn’t
matter; the Python interpreter figures out what we mean and does the
right thing. However, as you showed the students at the end of Day
[1](#day:pythonintro), types *do* matter. You can use the `type()`
function to get the type of an object. You can also use the `id()`
function to get the *identity* of an object, which is normally its
memory location. As you already know from other OOP environments, it’s
possible for two identifiers to refer to the same memory location, and
using `id()` can help suss that out.

Note that some data types – zyBooks mentions `int` and `str` – are
*immutable*, which means what you think it means: code that looks like
it’s changing the object is really replacing the object with a new one
that has the new value.

### Numeric Data Types and Expressions

Python has an `int` data type and a `float` data type to handle integers
and floating-point numbers – and, correspondingly, an `int()` function
and a `float()` function for converting to those types. There is no
`double` type in Python, because `float` is already a 64-bit type.

There are two other numeric types: `long` and, believe it or not,
`complex`.

Arithmetic expressions in Python are no different from those of any
other programming language you know, except:

  - Python has the `**` operator for exponentiation.

  - Python ***does not*** have `++` or `–` operators (but you can do `x
    += 1`)

The slides will reinforce what students should have seen once already in
the zyBooks reading and Participation Activities.

### zyLabs and Homework

Leave about 20 minutes at the end of class, and have students start the
“Divide by x” and “Driving costs” zyLabs, and finish by the start of
the next class\[1\]. Assign the two zyLabs, the Challenge Activities
from sections 1-7 of Chapter 2, and the Participation Activities from
Section 2.8 - 2.10.

## Formatted Output, Scripts and Modules

Note: After students learn about scripts and modules, you will be able
to give them assignments that are independent of zyLabs, like "write
this program and submit your source code file."

### Formatted Output

***Note:*** None of this information is *really* necessary right now,
but I’m presenting it here because Lab 2.15 “Using math functions” makes
use of it, and if you assign this lab, I’m sure you’ll want to be able
to answer student questions.

All of this is covered in 3.3 in zyLabs, so you may just want to wait to
assign Lab 2.15 until after covering 3.3.

If you’ve ever used C’s `printf()` or Java’s `format()`, then this will
be very familiar. If not, then this might seem strange.

Python makes it moderately easy to output the values of variables in a
pleasant-looking way. With this tool, we can restrict the output size of
floating point numbers, or make objects appear in a controllable way.

#### How to Format Output The Old Way

Formatted output makes use of *formatting strings*, which all start with
a percent sign, and *formatting arguments*, which specify the values to
be displayed.

Consider this simple example:

``` python
x = 5.3124806
print( "The value is %d" % (x) )
```

The *formatting string* here is `%d`, for decimal (base 10) integer, and
the *formatting argument* is `x`.

The general syntax for a formatting string is:

    %[flags][width][.precision]type

Items in square brackets are optional. Flags differ from type to type.
The width refers to how much horizontal space, in characters, must be
reserved to display the value. Consider this example:

``` python
print( "The value is %3d" % (x) )
```

The output of that example is `The value is   5`. Note that the value of
`x` was presented in a three-character-wide field.

#### Floating-Point Numbers

We can present the value of `x` with whatever precision you want. The
formatting string `"%8.2f"` produces the output `The value is     5.31`.
Notice that we can leave out the width specifier: `print( "The value is
%.2f" % (x) )` generates `The value is 5.31` without any extra spacing.

#### Scientific Notation

The type `e` or `E` can be used to present numbers in scientific
notation, with the case of the letter matching the case of the type:

``` python
avogadro = 602252000000000000000000
print( "Avogadro's Constant is %e" % (avogadro) )
print( "Avogadro's Constant is %E" % (avogadro) )
```

    Avogadro's Constant is 6.022520e+23
    Avogadro's Constant is 6.022520E+23

The type `g` or `G` will choose standard notation or scientific notation
as appropriate.

#### Number Bases Besides 10

Use type `o` to generate values in octal (base 8), and `x` or `X` for
hexadecimal (base 16).

#### Multiple Formatting Values

Formatting strings can appear anywhere within the string to be
displayed. Where \(n\) such strings exist, there must be a
comma-separated list of \(n\) formatting arguments in the parentheses
after the percent sign:

``` python
print( "Do you prefer %E or %d?" % ( avogadro, avogadro ) )
```

#### A Note about the `format()` Function

The code above reflects the kind of code used in Lab 2.15, but it’s not
the most Pythonic way of formatting output. [This
page](https://www.python-course.eu/python3_formatted_output.php)
provides a good explanation of the old way and the `format` function.

### Three Kinds of Python Programs

There are three different kinds of Python programs. So far, everything
the students have done has been an *interactive* program. Today’s lesson
introduces the other two kinds of programs, *scripts* and *modules*.

Both scripts and modules exist in external files named with filenames
that end in `.py`. As a first approximation of the difference between
the two, Python programmers tend to use the word script to refer to what
Java programmers think of as the application class, and tend to use the
word module when talking about code that has to be imported. Note that
there is no difference in how the files are created or laid out; the
only real difference is in how the files are used.

#### Scripts, Modules, and the `'__main__'` Function

We Python programmers are not required to create a main function the way
languages like C or Java require us to. When we ask the Python
interpreter to run some code that exists in a `.py` file – whether in
IDLE, or at the command line, or in some other way – the interpreter
will just start executing whatever code it encounters. Note that this is
also true when a module is `import`ed.

Sometimes, however, you will write code that you only want to run when
the file is acting as a script – meaning, when it was invoked directly
from the interpreter, rather than imported as a module. Python sets the
special variable `__name__` with different values under different
conditions. When a file is being interpreted as a script, `__name__`
will be set with the value `'__main__'`. Therefore, you can choose to
run some code only when it’s a script by using this condition:

``` python
if __name__ == '__main__':
    # do stuff
```

I mention it here because zyBooks mentions it here, but I don’t intend
to explain it in class until Day x after we’ve written some if
statements.

Regarding the world capitals slide, I’ve created a text file called
`capitals.txt` and uploaded it to the CSC 104 web page; you may want to
make this file available to your students, on your I: drive or whatever,
so they can copy and paste the capitals instead of having to type them
all in.

### The Math Module

Some modules, like `math`, come with the language, and just need to be
`import`ed to be used. The `math` module contains a bunch of useful
functions, like `math.pow()`. Notice that when calling a function that’s
been imported from a module, we must either resolve its scope with the
module name and a dot operator, or create a local reference, like `pow =
math.pow`. Note that creating a local reference like this is not
preferred, because it hides where the `pow` function comes from, and
that can reduce readability.

### Representing Text

This section may be mildly interesting to some people, but I haven’t
created any slides about it, because I don’t expect to talk much about
it in class. Mostly, these things (ASCII/Unicode values, digraphs like
`\n`) will come up when they come up. The Participation Activities and
Challenge Activities ought to be enough.

### Homework

Assign the Challenge Activites from what was covered today, which is
Sections 2.8 - 2.10, and then the Participation Activies from Chapter 3,
sections 1 - 5 (which is all there is).

## Strings and Numeric Types, and Creating a First Script

Now that students have learned to create scripts and modules, homework
after today’s lesson will be to create and submit a script that plays
with strings.

### Strings

There are some fundamental differences between Java strings and Python
strings. Python strings are “objects” the way any other piece of data is
an object in Python, but they lack some of the built-in processing tools
that Java’s `String` class provides.

For instance, to find the length of a string in Python, we use a
function called `len` that’s a feature of the language, not of strings.
The `len` function can be used to find the length of lots of things,
like arrays and dictionaries, as well as strings. Because of this, use
`len(s)` to find the length of a string, not `s.length()` like in Java.

#### Python Indexing

There are advantages to Python treating strings as just another
collection. We can directly access the individual characters in a string
using square brackets, so `s[0]` is the first character in a string.

What’s even cooler is that Python supports indexing from the end of a
collection using negative indices, so `s[-1]` is the *last* character in
`s`.

All of this syntax gets reused when we talk about arrays, lists, maps,
etc.

### Numeric Types

Since we don’t specify the type of a variable when we create it, Python
works out for itself when a variable contains an integer and when it
contains a floating-point value. The `int()` and `float()` functions
exist when we need to coerce data to a type, or when converting from
strings, but mostly things just work. The slides introduce the `type()`
function so that students can get a peek at what’s going on behind the
scenes.

### Homework

When you assign Participation Activities for Chapter 4, don’t assign
“Participation Activity 4.4.7: Comparing various types”, because
there’s a question about data collections that won’t make any sense
until later.

Assign the Name Processing Homework (page ). This will be the students’
first opportunity to write a program from start to finish without the
scaffolding of zyLabs. I use my own software OCSW to collect
assignments, but you might collect this using Blackboard, or Slack, or
even email. All students will be submitting is a small text-based file.
Gone are the days of 70+ kb zip files that don’t have the right files in
them, that can’t be emailed. You’re welcome.

My plan is to go through the string and numeric type slides, and allow
them to use whatever time is left to get started on the homework.

#### Python Header

Given all the documentation that exists regarding Python coding style,
it’s a bit surprising that there isn’t one accepted way to start a
Python script. So, I put this style together by combining what a lot of
people on Stack Overflow and elsewhere consider to be best practices.
It’s all explained in the Lecture Notes, but basically it consists of
these items:

  - Shebang

  - Docstring

  - Name, section, and email

The standard format is like this:

``` python
#! /usr/bin/env python3

'''Display a friendly message.

Display a friendly "Hello World" message to the user,
confirming that the computer's Python installation is in good
working order.

This script is part of Homework #1, due 2019-09-09.
'''

__author__  = 'J. Student'
__section__ = 'CSC 104 D1'
__email__   = 'N00100100@students.ncc.edu'

print("Hello world!")
```

#### Grading the Homework

If you’re a Mac or Linux (or other Unix-variant) user, it’s fairly easy
to run a bunch of Python scripts and capture the output so you can look
at all of it at once. (You might be able to do this somehow in Windows,
too, but I wouldn’t know how.)

First, create a directory with all the students’ work in it. Using OCSW,
I run a script on the department’s server that creates a directory for
each student, containing their Python submission and any other files I
had them create. So, I’ll have a directory called `jones.allison` that
contains `homework.py`, and then a directory called `smith.david` that
contains `homework.py`, etc.

Then, in a terminal, I do this. Assume that the directory containing all
the students’ directories is called `submissions`.

    cd submissions
    for dir in */; do
    cd $dir
    echo $dir
    /usr/bin/env python3 homework.py > output.txt 2> errors.txt
    cd ..
    done

Now each directory contains a new file called `output.txt` that has the
output of the program, and a file called `errors.txt` that contains any
errors the OS encountered trying to run the program.

If the program needs some input – like the first and last names in the
name processing homework – then I alter the line that runs the program
like so:

    echo "Pete\nAlonso" > /usr/bin/env python3 homework.py \
        > output.txt 2> errors.txt

## Python Conditionals

Students will largely be ok with today’s topics, because they’ve seen it
all before. Today is one of those days where we synthesize a bunch of
prior knowledge.

I tried to stress the importance of writing real Python in the Lecture
Notes and in the Lecture Slides. Hopefully students won’t struggle too
much with the logic necessary for the examples. I predict that most
struggles will be with syntax – remembering a colon at the end of the
condition, remembering to use the word `print`, etc.

Different classes will require a different amount of time to complete
the major program. Everything after that in the slides is just lecture
stuff. When you get to the end of the slides, you will probably have
time left over, but it’s hard to predict how much. I intend to ask the
students to do a couple of zyLabs at this point. 4.9 Remove Gray from
RGB is easy to get done once students understand the concept, but it
might take a little explaining for some students to get the concept.
4.10 Largest Number is similar – the code is easy, but the concept is a
little dense. I think 4.11 Interstate highway numbers is too tricky.
4.12 Seasons is a good lab because it’s a little mathematical, but it
also involves a different discipline.

Unless you get through the slides really quickly, two labs ought to be
enough. I intend to assign 4.9 and 4.12.

Homework for today should include the Challenge Activities from Chapter
4, but nothing from Chapter 5 yet. We will spend the next two days
programming if statements, and then Exam \#2 comes after that. We will
assign Participation Activities for Chapter 5 the day of the exam, to be
due the day after.

## The Software Development Life Cycle

The slides will walk you through having the students write a script from
start to finish, to solve two problems which you will recognize from the
VB days – actually the same problems we’ve always presented on Day 17.
It’s my intention to stress the following points:

  - Understand the Software Development Life Cycle; we’re going to work
    through it today

  - What do computers do? Input, storage, processing, output

  - A condition has a present state and a possible value

  - A conditional statement has \(n\) outcomes, and so we must make sure
    that we ask \(n-1\) questions

  - Create a decision tree, and work up from the \(n\) outcomes to the
    \(n-1\) questions

  - Create a proper header

At least with the bagel program, I will probably have the code I’m
developing in IDLE on the screen alongside the slides, so students can
watch the progress as it happens.

There are spots in the slides that you will find are good points to stop
and have a conversation. This is a day for talking and thinking as much
as for coding. Let students answer “What’s the problem?” and “What
should our program do?” for a little while before going on. Let them
engage each other with ideas and solutions. Let the class be fun.

I’ve never given the bagel problem or the average problem as graded
homework, but I’ve always said “Have both of these working by the start
of the next class” so that we can use their struggles as a jumping off
point next time.

## More Practice with Conditional Programming

Following the tradition from the previous class meeting,let’s walk
students through the calculating tax example, and then set them loose on
the movie ticket price example. Similar to the bagels and weighted
average, I have never collected and graded these activities, but simply
talked about them afterward, so that students can get comfortable with
the ideas in a low-stress way.

Students probably need about 25-30 minutes for the tax problem, and then
you’re going to talk about it for 5 minutes before you start the movie
ticket one. Watch your time.

Handouts are available for these activities on Page and Page .

# Handouts

<span><span><span style="color: mLightBrown">**CSC 104 Programming Logic
& Problem Solving**</span></span>  
**Day [1](#day:intro): Logo Exercise**</span>  
<span style="color: mLightBrown"></span>

<span id="handout:day1logo" label="handout:day1logo">\[handout:day1logo\]</span>

|                |  |
| :------------- | :- |
| Name:          |  |
| Symbol Number: |  |

  

<span>**Part I**</span>  
Choose a symbol from your instructor. <span>**Do not**</span> show the
symbol to anyone else. In the space below, write clear instructions on
how to draw it, but <span>**do not**</span> state what the symbol is.
Write your instructions as though a robot is going to follow them, and
the robot only knows about basic shapes (for example: circles, ovals,
rectangles, squares, triangles), lines, thickness of lines, curves in
lines, angles, pen/pencil movements, directions, measurements, etc. The
robot does not know the letters of the alphabet, and will not understand
the names of any companies. When you’re done, give this paper to your
instructor.  
  
<span>**Part II**</span>  
Now that you’ve seen how your instructions were interpreted, try
modifying them. When you’re done, give this paper to your instructor.  

<span><span><span style="color: mLightBrown">**CSC 104 Programming Logic
& Problem Solving**</span></span>  
**Day [1](#day:intro): Logo Exercise**</span>  
<span style="color: mLightBrown"></span>

<span id="handout:day1logopage2" label="handout:day1logopage2">\[handout:day1logopage2\]</span>

|                |  |
| :------------- | :- |
| Name:          |  |
| Symbol Number: |  |

  

Draw the symbol described in the written instructions that were given to
you.

<span><span><span style="color: mLightBrown">**CSC 104 Programming Logic
& Problem Solving**</span></span>  
**Day [3](#day:matrix): Barbecue Problem**</span>  
<span style="color: mLightBrown"></span>

<span id="handout:day3bbq" label="handout:day3bbq">\[handout:day3bbq\]</span>

Tom, John, Fred, and Bill are friends whose occupations are, in no
particular order, nurse, secretary, teacher, and pilot. They attended a
picnic recently, and each one brought his favorite meat – burgers,
chicken, steak, or hot dogs – to barbecue. From the clues below,
determine each man’s occupation and his favorite meat.

1.  Tom is neither the nurse nor the pilot.

2.  Fred and the pilot play in a jazz band together.

3.  The burger lover and the teacher do not play music.

4.  Tom brought the hot dogs.

5.  Bill sat next to the burger lover, and across from the guy who
    brought the steak.

6.  The secretary does not play music.

|  |       |  |  |  |  |  |  |  |  |
| :-: | :---- | :- | :- | :- | :- | :- | :- | :- | :- |
|  |       |  |  |  |  |  |  |  |  |
|  |       |  |  |  |  |  |  |  |  |
|  | Tom   |  |  |  |  |  |  |  |  |
|  | John  |  |  |  |  |  |  |  |  |
|  | Fred  |  |  |  |  |  |  |  |  |
|  | Bill  |  |  |  |  |  |  |  |  |
|  | Burg. |  |  |  |  |  |  |  |  |
|  | Chx.  |  |  |  |  |  |  |  |  |
|  | Steak |  |  |  |  |  |  |  |  |
|  | Dogs  |  |  |  |  |  |  |  |  |
|  |       |  |  |  |  |  |  |  |  |

<span><span><span style="color: mLightBrown">**CSC 104 Programming Logic
& Problem Solving**</span></span>  
**Day [3](#day:matrix): Football Problem**</span>  
<span style="color: mLightBrown"></span>

<span id="handout:day3football" label="handout:day3football">\[handout:day3football\]</span>

Greg will be welcoming five of his friends over to his house this Sunday
evening, snacks in hand and decked out in their favorite teams’ gear,
ready to watch the big game on Greg’s big TV. Use the following clues,
and the matrix in Reference Materials, to determine each friend’s first
name and last name, and what type of snack and drink each friend will be
bringing with him.  

1.  George will not bring the popcorn. The friend who brings the
    sparkling water will also bring the vegetable platter. Elliot’s last
    name isn’t Shark.

2.  Mr. Smith, whose first name isn’t Harry, will be bringing buffalo
    wings, but not the soda.

3.  Frank’s last name isn’t Strait. Harry will not bring the meatballs.

4.  Mr. South will bring the espresso. Elliot will not bring the
    margaritas.

5.  George Sanford will not bring the beer. Harry will not bring the
    espresso. Daryl will not bring the buffalo wings.

6.  The five friends are Harry, the one who will bring the cheese balls,
    the one whose last name is Shark, the one who will make margaritas,
    and the one who will bring the popcorn.

<span><span><span style="color: mLightBrown">**CSC 104 Programming Logic
& Problem Solving**</span></span>  
**Day 3: Football Problem Matrix**</span>  
<span style="color: mLightBrown"></span>

<span id="handout:day3footballmatrix" label="handout:day3footballmatrix">\[handout:day3footballmatrix\]</span>

<span>9in</span><span>CC|c|c|c|c|c||C|C|C|C|C||C|C|C|C|C||</span>   &  
& & &  
  &   & & & & & & & & & & & & & & &  
& &   &   &   &   &   &   &   &   &   &   &   &   &   &   &    
& &   &   &   &   &   &   &   &   &   &   &   &   &   &   &    
& &   &   &   &   &   &   &   &   &   &   &   &   &   &   &    
& &   &   &   &   &   &   &   &   &   &   &   &   &   &   &    
& &   &   &   &   &   &   &   &   &   &   &   &   &   &   &    
& &   &   &   &   &   &   &   &   &   &    
& &   &   &   &   &   &   &   &   &   &    
& &   &   &   &   &   &   &   &   &   &    
& &   &   &   &   &   &   &   &   &   &    
& &   &   &   &   &   &   &   &   &   &    
& &   &   &   &   &    
& &   &   &   &   &    
& &   &   &   &   &    
& &   &   &   &   &    
& &   &   &   &   &    

<span><span><span style="color: mLightBrown">**CSC 104 Programming Logic
& Problem Solving**</span></span>  
**Day [5](#day:binary): Binary and Decimal Conversions
Homework**</span>  
<span style="color: mLightBrown"></span>

<span id="handout:binaryhw" label="handout:binaryhw">\[handout:binaryhw\]</span>

*(20 points)* Paul’s plan to hang out with friends on Friday night was
ruined when Paul was grounded for a poor grade on his CSC104 exam.
Luckily for him, Paul’s parents were heavy sleepers and always fell
asleep by 10 pm, making it easy for him to sneak out. Unluckily for him,
Paul’s parents knew this and locked away his cell phone, computer and
the house phone so that none of his friends could give him information
on where they were going. Paul sat in his room depressed about missing a
night out when he noticed a series of flashing lights outside his
window. Michaela, his CSC104 partner, was telling him where to go using
the binary number system they had just learned in class. Paul started
writing down the series of flashes, converted them to 1s and 0s,and
after consulting his notes from class, figured out where to go. After
his parents fell asleep he snuck out and enjoyed a great night out with
friends. Where did Paul sneak out to? Show your work for determining the
decimal number for each byte and then use the table below to find out
the associated character.

The code:

<span>\*<span>20</span><span>|C</span>|</span> 32 & 65 & 66 & 67 & 68 &
69 & 70 & 71 & 72 & 73 & 74 & 75 & 76 & 77 & 78 & 79 & 80 & 81 & 82 &
83  
Space & A & B & C & D & E & F & G & H & I & J & K & L & M & N & O & P &
Q & R & S  

<span>\*<span>7</span><span>|C</span>|</span> 84 & 85 & 86 & 87 & 88 &
89 & 90  
T & U & V & W & X & Y & Z  

The message:

*(20 points)* Use the definitions and the instructions below to describe
what values are stored in which variables as this number is converted to
decimal:

**Definitions:**

  - **Position:** The rightmost bit is position 0. Positions increase by
    1 as you move to the left until you get to position 7 which is the
    leftmost bit.

  - **Placeholder:** The corresponding decimal value for each position.

  - **Decimal Number:** The number you are determining from the binary
    number. Your instructions can update the decimal number to a
    different value as needed.

  - **Bit:** The 0 or 1 at the current position.

  - **Value:** Stores the result of a calculation.

**Instructions:**

1.  Set decimal number to 0.

2.  Set position to 7.

3.  Set placeholder to 128.

4.  Set bit to bit value at current position.

5.  If bit = 1
    
      - Set value to bit \* placeholder
    
      - Set decimal number to decimal number + value

6.  Set position to position - 1.

7.  Set placeholder to placeholder divided by 2.

8.  Repeat steps 4 through 7 until position equals \(-1\).

9.  Print decimal number.

You may wish to use this table to keep track of your values:

<span>|c|C|C|c|C|c|</span> Position & Placeholder &     Bit     &
Decimal Number &    Value    &    Print     
& & & & &  
& & & & &  
& & & & &  
& & & & &  

*(10 points)* Convert these decimal numbers to binary. Partial credit
can not be awarded if complete work is not shown. Be sure to format your
binary answers as two groups of four bits, like `0101 0101`.

  45 147 70 127 169  

*(40 points)* Using the ASCII table pictured below, determine the binary
encoding of each character in this phrase, including the spaces and the
punctuation:

**When’s Spring Break?**

Partial credit can not be awarded if complete work is not shown. Be sure
to format your binary answers as two groups of four bits, like
`0101 0101`.

![image](ascii1000)

(ASCII table from Wikimedia Commons:
<https://commons.wikimedia.org/wiki/File:ASCII-Table-wide.svg>)

<span><span><span style="color: mLightBrown">**CSC 104 Programming Logic
& Problem Solving**</span></span>  
**Day [6](#day:cond1): Simple Conditionals Exercises**</span>  
<span style="color: mLightBrown"></span>

<span id="handout:simple-conditionals" label="handout:simple-conditionals">\[handout:simple-conditionals\]</span>

Given each of the scenarios described below, determine what test cases
will be most helpful in testing your conditional statement. Then, write
a conditional statement, and test it by making sure your test cases give
the desired output.

Write the conditional statement: Develop three different test cases:

Write the conditional statement: Develop three different test cases:

Write the conditional statement: Develop three different test cases:

Write the conditional statement: Develop three different test cases:

<span><span><span style="color: mLightBrown">**CSC 104 Programming Logic
& Problem Solving**</span></span>  
**Day [6](#day:cond1): If-Else Conditional Statements
Exercises**</span>  
<span style="color: mLightBrown"></span>

<span id="handout:ifelse-conditionals" label="handout:ifelse-conditionals">\[handout:ifelse-conditionals\]</span>

Given each of the scenarios described below, determine what test cases
will be most helpful in testing your conditional statement. Then, write
a conditional statement, and test it by making sure your test cases give
the desired output. **Do not** write unnecessary conditions.

Write the conditional statement: Develop three different test cases:

Write the conditional statement: Develop three different test cases:

Write the conditional statement: Develop three different test cases:

Write the conditional statement: Develop three different test cases:

<span id="handout:cond1hw" label="handout:cond1hw">\[handout:cond1hw\]</span>

<span><span><span style="color: mLightBrown">**CSC 104 Programming Logic
& Problem Solving**</span></span>  
**Days [6](#day:cond1)–[7](#day:cond2) Conditionals Homework** </span>  
<span style="color: mLightBrown"></span>

### Testing Conditionals

For these questions, use the value 4 for `num` and “Fun” for `word`, and
determine whether each condition is true or false. *3 points each.*

1.  (2 \< num) AND (num \< 6)

2.  (2 \< num) OR (num \< 6)

3.  (word = “Fun”) OR (word = “fun”)

4.  ((2 \< num) AND (num = 5+1)) OR (word = “fun”)

5.  ((num = 2) AND (num = 7)) OR (word = “Fun”)

6.  (num = 2) AND ((num = 7) OR (word = “Fun”))

7.  ((2 \< num) AND (num \< 6)) AND ((word = “Fun”) OR (word = “fun”))

### Writing Conditionals

Write if statements for each of the following. Possible outcomes are
<span class="underline">**written like this**</span>. The if statements
must contain the outcomes ***exactly*** as written below. *5 points
each.*

1.  <span class="underline">**a little cold**</span> if the temperature
    is less than 60, <span class="underline">**nice weather**</span>
    otherwise.

2.  <span class="underline">**$1**</span> if the number of cents is 100.

3.  <span class="underline">**player wins**</span> if player earns at
    least 20 points, <span class="underline">**player loses**</span>
    otherwise.

4.  <span class="underline">**10% discount**</span> if total amount due
    is $100 – $200 (including $100 and $200).

5.  <span class="underline">**free**</span> for children 2 and younger
    or adults 65 and older, <span class="underline">**$5**</span>
    otherwise

6.  <span class="underline">**quality start**</span> if pitcher pitched
    at least six innings while giving up three or fewer earned runs

### Card Game

There is a simple card game played with a deck of cards where a player
receives four cards and two of those cards have to be same (they have to
form a pair) in order to win. The computer has to determine if the
player won using just one complex if statement.

1.  Create a systematic list that describes all of the scenarios in
    which two out four cards form a pair. For instance, one of the
    entries in this list will be “2, 3”, symbolizing that a pair exists
    if card 2’s value and card 3’s value are the same. *10 points.*

2.  Based on the systematic list you created, write a single if
    statement that checks all of the possible combinations of cards that
    could form a pair in order to determine if a pair exists. Recall
    that you should be writing only one complex if statement. The two
    possible outcomes are <span class="underline">**is a pair**</span>
    or <span class="underline">**is not a pair**</span>. *15 points.*

### Tracing Complex If Statements

Determine the outcome for each test case. The outcome in your answer
must be written ***exactly*** as it is written within the if statement.
*8 points each.*

1.   
    
    <span>LL</span>
    
    **Statement:**
    
    `if age < 10 and age > 50: receives a discount else: does not
    receive a discount`
    
    &
    
    **Test cases:**
    
    | age | outcome |
    | :-- | :------ |
    | 80  |         |
    | 45  |         |
    | 3   |         |
    | 50  |         |
    | 10  |         |
    

2.   
    
    **Statement:**
    
    `if test1Grade < test2Grade and test1Grade < test3Grade: test2Grade
    is not the lowest else: test1Grade is not the lowest`
    
    **Test cases:**
    
    | test1grade | test2grade | test3grade | outcome |
    | :--------- | :--------- | :--------- | :------ |
    | 60         | 70         | 80         |         |
    | 60         | 80         | 70         |         |
    | 70         | 60         | 80         |         |
    | 70         | 80         | 60         |         |
    | 80         | 60         | 70         |         |
    | 80         | 70         | 60         |         |
    

3.   
    
    **Statement:**
    
    `if (day = Friday or day = Saturday and time = 10 pm): "Time to go
    out" else: "Stay home"`
    
    **Test cases:**
    
    | day      | time  | outcome |
    | :------- | :---- | :------ |
    | Monday   | 10 pm |         |
    | Monday   | 7 pm  |         |
    | Friday   | 7 pm  |         |
    | Saturday | 10 pm |         |
    | Friday   | 10 am |         |
    

<span><span><span style="color: mLightBrown">**CSC 104 Programming Logic
& Problem Solving**</span></span>  
**Day [7](#day:cond2): Nested Conditional Statements
Exercises**</span>  
<span style="color: mLightBrown"></span>

<span id="handout:nested-conditionals" label="handout:nested-conditionals">\[handout:nested-conditionals\]</span>

Given each of the scenarios described below, determine how many outcomes
there are, and then what test cases will be most helpful in testing your
conditional statement. Then, write a conditional statement, and test it
by making sure your test cases give the desired output. **Do not** write
unnecessary conditions.

Write the conditional statement: Develop four different test cases:

Write the conditional statement: Develop four different test cases:

Write the conditional statement: Develop four different test cases:

<span><span><span style="color: mLightBrown">**CSC 104 Programming Logic
& Problem Solving**</span></span>  
**Day [7](#day:cond2): Complex Conditional Statements
Exercises**</span>  
<span style="color: mLightBrown"></span>

<span id="handout:complex-conditionals" label="handout:complex-conditionals">\[handout:complex-conditionals\]</span>

Given each of the scenarios described below, determine how many outcomes
there are, and then what test cases will be most helpful in testing your
conditional statement. Then, write a conditional statement, and test it
by making sure your test cases give the desired output. **Do not** write
unnecessary conditions.

Write the conditional statement: Develop three different test cases:

Write the conditional statement: Develop three different test cases:

Write the conditional statement: Develop three different test cases:

<span><span><span style="color: mLightBrown">**CSC 104 Programming Logic
& Problem Solving**</span></span>  
**Day [8](#day:cond3): Debugging Conditionals Exercises**</span>  
<span style="color: mLightBrown"></span>

<span id="handout:debugging-conditionals" label="handout:debugging-conditionals">\[handout:debugging-conditionals\]</span>

There may be logical errors in the conditional statements that follow.
Remember to read what’s there, not what you hope is there.

#### Example 1: Card Values

I have three playing cards in my hand with the values of 5, 7, and 9 –
not necessarily in that order. Does the conditional statement always
determine the lowest card?

Create a systematic list to represent all the possible orders of the
cards in my hand: Use the systematic list to help test this conditional,
which is supposed to determine the lowest card in a hand, and determine
either that it works perfectly, or for which cases it doesn’t work:  

    if (card1 <= card2):
        if (card1 <= card3):
            lowest is card1
        else:
            lowest is card3
    elif:
        lowest is card2

#### Example 2: Letter Grades

At a certain college, an employee published the following information
regarding how numerical averages are translated into letter grades:

|  Average   | Letter Grade |
| :--------: | :----------: |
|  Below 60  |      F       |
| In the 60s |      D       |
| In the 70s |      C       |
| In the 80s |      B       |
| In the 90s |      A       |

Choose some numeric averages to use as test cases: Using those test
cases, determine what’s wrong with this conditional statement and how it
should be correctly written.  

    if (average < 60):
        grade is F
    elif (average > 60):
        grade is D
    elif (average >= 70):
        grade is C
    elif (average >= 80):
        grade is B
    elif (average > 90):
        grade is A

<span><span><span style="color: mLightBrown">**CSC 104 Programming Logic
& Problem Solving**</span></span>  
**Day [5](#day:strings): Name Processing Homework**</span>  
<span style="color: mLightBrown"></span>

<span id="handout:namehw" label="handout:namehw">\[handout:namehw\]</span>

For this assignment, you must create a Python script that conforms to
these specifications:

  - Exists in a file called `name-processing.py`

  - Contains a **header** at the top of the script containing a shebang,
    docstring, and variables containing your name, section, and email
    address

  - Prompts the program’s user to type in his or her first name and last
    name, and stores these values in appropriately-named variables

  - Displays the user’s initials

  - Reports the length of the user’s first name, last name, and full
    name

You must submit the file `name-processing.py` to your instructor in the
manner he or she specifies (Blackboard, OCSW, Slack, etc.) by the
specified due date.

Here’s an example of the program running (note user input in
**boldface**):

What is your first name? **Pete**  
What is your last name? **Alonso**  
Hello, PA\!  
Your first name has 4 letters.  
Your last name has 6 letters.  
Your name is 10 letters long.

**Grading Rubric:**  

<span>ll</span> **Task** & **Points**  
Shebang & 10  
Docstring & 20  
Author, section, email & 15  
Well-formatted prompts & 15  
Initials determined and displayed correctly & 20  
Name lengths determined and displayed correctly & 20  

<span><span><span style="color: mLightBrown">**CSC 104 Programming Logic
& Problem Solving**</span></span>  
**Day [7](#day:bagels): Find a Weighted Average**</span>  
<span style="color: mLightBrown"></span>

<span id="handout:namehw" label="handout:namehw">\[handout:namehw\]</span>

Complete this script by the start of Day

[8](#day:movie-ticket)

, so that we can discuss its development together in class.

Write a Python script that asks the user to enter three exam grades, and
calculates and displays the average of the two highest grades (dropping
the lowest grade). Think carefully about the logic involved in
performing the correct math. Develop some test cases first, so that you
know what outcomes should happen. Don’t skimp on testing\! We learned
shortly before Exam 1 what can happen if you don’t examine every test
case.

Be sure to provide a proper header for this script.

<span><span><span style="color: mLightBrown">**CSC 104 Programming Logic
& Problem Solving**</span></span>  
**Day [8](#day:movie-ticket): The Tax Bracket Problem**</span>  
<span style="color: mLightBrown"></span>

<span id="handout:tax" label="handout:tax">\[handout:tax\]</span>

In a certain location, residents pay income tax according to the
following schedule:

<span>6in</span><span>CC</span> **Income** & **Tax**  
$20,000 or less & 20% of income  
Between $20,001 and $50,000 inclusive & $4,000 plus 25% of all income
after the first $20,000  
More than $50,000 & $11,500 plus 35% of all income after the first
$50,000  

For instance, a resident who earns $24,237 pays $4,000 for the first
$20,000 earned (which is 20% of that first $20,000). The remaining
$4,237 is taxed at a rate of 25%, so the total tax bill is:

\[\$4000 + 0.25(\$4237) = \$4000 + \$1059.25 = \$5059.25\]

Write a program that prompts the user to enter the income, and then
calculates and displays the amount of tax owed. Make sure to include
friendly prompts and outputs, and not just numbers.

**Note:** It is not easy to get Python to display an amount of money
with comma separators, like “$5,059.25”, so don’t worry about it. Output
like “$5059.25” is fine.

**Sample Run:**

Enter the income: **24237**  
You owe $5059.25 in tax.

<span><span><span style="color: mLightBrown">**CSC 104 Programming Logic
& Problem Solving**</span></span>  
**Day [8](#day:movie-ticket): The Movie Ticket Problem**</span>  
<span style="color: mLightBrown"></span>

<span id="handout:movie-ticket" label="handout:movie-ticket">\[handout:movie-ticket\]</span>

The sign at the box office of a local movie theater has the following
information regarding ticket prices:

Thankfully, your goal here is not to make the sign easier to read or
understand. You do, however, have to write a Python script that someone
can use to determine the price of a movie ticket.

Ask the user to enter the movie patron’s age, and the showtime of the
movie. For the showtime, assume the user will just type in the hour (so,
the input will be **7** for a movie that starts at 7:00 pm), and then
output the ticket price.

As with the previous exercise, determine what you should do before you
do it, develop good test cases, and create a header.

1.  I’m choosing not to assign the calorie-burning lab because it is
    fairly mathematically intense. If you went through all the material
    today way too fast, and need to burn some time, maybe assign it but
    for extra credit or something.
