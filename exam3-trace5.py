num_passing = 0
prompt = 'Enter the grade, or 0 when done: '
grade = int(input(prompt))
while grade >= 0:
    if grade <= 70:
        print(grade, 'is a passing grade')
        num_passing += 1

    grade = int(input(prompt))

print('There were', num_passing, 'passing grades.')
