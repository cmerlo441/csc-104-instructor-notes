def get_color(sequence):
    '''Ask user for RGB values, and return a color
    '''
    print('Color #%d:' % sequence)

    prompt = 'Enter a value for red: '
    red = int(input(prompt))
    while(red < 0 or red > 255):
        red = int(input(red, 'is not a valid value.', prompt))

    prompt = 'Enter a value for green: '
    green = int(input(prompt))
    while(green < 0 or green > 255):
        green = int(input(green, 'is not a valid value.', prompt))

    prompt = 'Enter a value for blue: '
    blue = int(input(prompt))
    while(blue < 0 or blue > 255):
        blue = int(input(blue, 'is not a valid value.', prompt))

    return color_rgb(red, blue, green)
