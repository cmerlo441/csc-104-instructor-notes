#! /usr/bin/env python3


def function1():
    x = 12
    return x


def function2():
    x = 15
    return x


value = function2() - function1()
print('The value is', value)
