#! /usr/bin/env python3


def larger(x, y):
    if(x > y):
        return x
    else:
        return y


print(larger(3, 5))
print(larger(7, 2))
print(larger(4, 4))
