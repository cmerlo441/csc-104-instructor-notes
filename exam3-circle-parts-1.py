# find the ticket price
def determine_price(x):
    if x >= 65:
        return 10
    else:
        return 20

base_cost = 5
age = int(input('Enter your age: '))
price = determine_price(age)
total = price + base_cost
print('Your cost is:', total)
