#! /usr/bin/env python3

'''Day 26 Graphics Programming Activity

Create two circles side-by-side with user-specified colors

This version has lots of repeated code, but it approximates
what the students might accomplish first.
'''

__author__ = 'Prof. Christopher R. Merlo'
__email__ = 'cmerlo@ncc.edu'


from graphics import *
width = 600
height = 300

win = GraphWin('Colorful Circles', width, height)

x1 = width / 4
x2 = 3 * width / 4
y = height / 2

p1 = Point(x1, y)
p2 = Point(x2, y)

c1 = Circle(p1, y)
c2 = Circle(p2, y)

c1.draw(win)
c2.draw(win)

# Enter Color #1

print('Color #1:')
prompt = 'Enter a value for red: '
red1 = int(input(prompt))
while(red1 < 0 or red1 > 255):
    red1 = int(input(red1, 'is not a valid value.', prompt))


prompt = 'Enter a value for green: '
green1 = int(input(prompt))
while(green1 < 0 or green1 > 255):
    green1 = int(input(green1, 'is not a valid value.', prompt))

prompt = 'Enter a value for blue: '
blue1 = int(input(prompt))
while(blue1 < 0 or blue1 > 255):
    blue1 = int(input(blue1, 'is not a valid value.', prompt))

# Enter Color #2

print('Color #2:')
prompt = 'Enter a value for red: '
red2 = int(input(prompt))
while(red2 < 0 or red2 > 255):
    red2 = int(input(red2, 'is not a valid value.', prompt))


prompt = 'Enter a value for green: '
green2 = int(input(prompt))
while(green2 < 0 or green2 > 255):
    green2 = int(input(green2, 'is not a valid value.', prompt))

prompt = 'Enter a value for blue: '
blue2 = int(input(prompt))
while(blue2 < 0 or blue2 > 255):
    blue2 = int(input(blue2, 'is not a valid value.', prompt))

# Enter Color #3

print('Color #3:')
prompt = 'Enter a value for red: '
red3 = int(input(prompt))
while(red3 < 0 or red3 > 255):
    red3 = int(input(red3, 'is not a valid value.', prompt))


prompt = 'Enter a value for green: '
green3 = int(input(prompt))
while(green3 < 0 or green3 > 255):
    green3 = int(input(green3, 'is not a valid value.', prompt))

prompt = 'Enter a value for blue: '
blue3 = int(input(prompt))
while(blue3 < 0 or blue3 > 255):
    blue3 = int(input(blue3, 'is not a valid value.', prompt))

# Enter Color #4

print('Color #4:')
prompt = 'Enter a value for red: '
red4 = int(input(prompt))
while(red4 < 0 or red4 > 255):
    red4 = int(input(red4, 'is not a valid value.', prompt))

prompt = 'Enter a value for green: '
green4 = int(input(prompt))
while(green4 < 0 or green4 > 255):
    green4 = int(input(green4, 'is not a valid value.', prompt))

prompt = 'Enter a value for blue: '
blue4 = int(input(prompt))
while(blue4 < 0 or blue4 > 255):
    blue4 = int(input(blue4, 'is not a valid value.', prompt))

# Create colors

color1 = color_rgb(red1, blue1, green1)
color2 = color_rgb(red2, blue2, green2)
color3 = color_rgb(red3, blue3, green3)
color4 = color_rgb(red4, blue4, green4)

# Switch colors

i = 0
while(i < 3):

    c1.setOutline('black')
    c1.setFill('white')
    c2.setOutline('black')
    c2.setFill('white')

    win.getMouse()

    c1.setOutline(color1)
    c1.setFill(color2)
    c2.setOutline(color3)
    c2.setFill(color4)

    win.getMouse()

    i += 1


win.close()
