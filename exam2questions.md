# CSC 104 Exam Questions

## Multiple Choice
1. What is the purpose of a comment?
    1. To allocate memory
    1. To assign a value to a variable
    1. To decide what code to run based on a condition
    1. To provide information to someone reading the source code
1. Consider these two Python statements:  x = 4 and then y = x + 12 / 2 What will be stored in y?
    1. 6
    1. 8
    1. 10
    1. 12
1. Which of these is the best variable name for a Python program?
    1. pricePerPerson
    1. PricePerPerson
    1. price_per_person
    1. Price per person
1. Which of these reads in input from the user and stores it as a number?
    1. x = input(’Type in a number: ‘)
    1. x = input(float(’Type in a number: ‘))
    1. float(input(’Type in a number: ‘)) = x
    1. x = float(input(’Type in a number: ‘))
