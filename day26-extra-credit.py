#! /usr/bin/env python3

'''Day 26 Graphics Programming Activity

Create two circles side-by-side with user-specified colors

This version is as short as we can get it without
learning about collections.
'''

__author__ = 'Prof. Christopher R. Merlo'
__email__ = 'cmerlo@ncc.edu'


from graphics import *
width = 600
height = 300

win = GraphWin('Colorful Circles', width, height)

x1 = width / 4
x2 = 3 * width / 4
y = height / 2

p1 = Point(x1, y)
p2 = Point(x2, y)

c1 = Circle(p1, y)
c2 = Circle(p2, y)

c1.draw(win)
c2.draw(win)


def get_color(sequence):
    '''Ask user for RGB values, and return a color
    '''
    print('Color #%d:' % sequence)

    prompt = 'Enter a value for red: '
    red = int(input(prompt))
    while(red < 0 or red > 255):
        red = int(input(red, 'is not a valid value.', prompt))

    prompt = 'Enter a value for green: '
    green = int(input(prompt))
    while(green < 0 or green > 255):
        green = int(input(green, 'is not a valid value.', prompt))

    prompt = 'Enter a value for blue: '
    blue = int(input(prompt))
    while(blue < 0 or blue > 255):
        blue = int(input(blue, 'is not a valid value.', prompt))

    return color_rgb(red, blue, green)


# Create colors

color1 = get_color(1)
color2 = get_color(2)
color3 = get_color(3)
color4 = get_color(4)

# Switch colors

i = 0
while(i < 3):

    c1.setOutline('black')
    c1.setFill('white')
    c2.setOutline('black')
    c2.setFill('white')

    win.getMouse()

    c1.setOutline(color1)
    c1.setFill(color2)
    c2.setOutline(color3)
    c2.setFill(color4)

    win.getMouse()

    i += 1


win.close()
