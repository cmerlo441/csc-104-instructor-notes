#! /usr/bin/env python3

now = int(input("What is today's date? "))
rent = int(input('When is the rent due? '))

difference = rent - now

if(difference > 0):
    print('The rent is due in', difference, 'days.')
else:
    print('I hope you paid the rent already!')