# CSC 104 Binary and Decimal Conversions Homework Answers

## Paul's Plan (20 points)
JIMS HOUSE

## Follow Binary-to-Decimal Algorithm (20 points)
| Position | Placeholder | Bit | Decimal Number | Value | Print |
| --- | --- | --- | --- | --- | --- |
| | | | 0 | |
| 7 | 128 | 1 |  | 128 | |
| | | | 128 | |
| 6 | 64 | 0  | | |
| 5 | 32 | 1 | 32 | 160 | |
| 4 | 16 | 1 | 16 | 176 | |
| 3 | 8 | 0 |
| 2 | 4 | 0 |
| 1 | 2 | 1 | 2 | 178 | |
| 0 | 1 | 1 | 1 | 179 | 179 |

## Convert to Binary (20 points)
45 = 0010 1101

147 = 1001 0011

70 = 0100 0110

127 = 0111 1111

169 = 1010 1001

## When's Spring Break?

### When's
```
0101 0111
0110 1000
0110 0101
0110 1110
0001 1011
0111 0011
```

### Spring
```
0010 0000
0101 0011
0111 0000
0111 0010
0110 1001
0110 1110
0110 0111
```

### Break
```
0010 0000
0100 0010
0111 0010
0110 0101
0110 0001
0110 1011
0011 1111
```