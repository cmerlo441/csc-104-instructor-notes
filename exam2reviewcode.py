#! /use/bin/env3 python

'''Some code.

This is some code to examine while reviewing for Exam #2.
'''

from graphics import *

win = GraphWin('Good luck on exam #2!', 250, 250)

x = int(input('What should the x value of your point be? '))
y = int(input('What should the y value of your point be? '))

my_point = Point(x, y)

print('Do you see your point at (' + x + ', ' + y + ') yet?')
