# Days 6-7 Conditionals Homework

## Testing Conditionals (3 points each)

1. True
2. True
3. True
4. False
5. True
6. False
7. True

## Writing Conditionals (5 points each)
1.
    ```python
    if(temperature < 60):
        a little code
    else:
        nice weather
    ```

2.
    ```python
    if(number of cents = 100):
        $1
    ```

3.
    ```python
    if(points >= 20):
        player wins
    else:
        player loses
    ```

4.
    ```python
    if(amount due >= 100 and amount due <= 200):
        10% discount
    ```

5.
    ```python
    if(age < 2 or age >= 65 ):
        free
    else:
        $5
    ```

6.
    ```python
    if(innings >= 6 and earned runs <= 3):
        quality start
    ```

## Card Game
### Systematic List (10 points)
[1,2], [1,3], [1,4], [2,3], [2,4], [3,4]

### If Statement (15 points)
```python
if(card1 == card2 or card1 == card3 or card1 == card4 or [...]):
    is a pair
else
    not a pair
```

## Tracing Complex Statements (8 points each)
1. Everything is false, so "does not receive a discount" for all
2.
    | test1grade | test2grade | test3grade | outcome |
    | -- | -- | -- | -- |
    | 60 | 70 | 80 | 2 |
    | 60 | 80 | 70 | 2 |
    | 70 | 60 | 80 | 1 |
    | 70 | 80 | 60 | 1 |
    | 80 | 60 | 70 | 1 |
    | 80 | 70 | 60 | 1 |

3. home / home / home / go out / home