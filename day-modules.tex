% !TEX root = CSC104InstructorNotes.tex

\chapter{Formatted Output, Scripts and Modules}
\label{day:modules}

Note: After students learn about scripts and modules, you will be able to give them assignments that are independent of zyLabs, like "write this program and submit your source code file."

\section{Formatted Output}

\textbf{\textit{Note:}} None of this information is \textit{really} necessary right now, but I'm presenting it here because Lab 2.15 ``Using math functions'' makes use of it, and if you assign this lab, I'm sure you'll want to be able to answer student questions.

All of this is covered in 3.3 in zyLabs, so you may just want to wait to assign Lab 2.15 until after covering 3.3.

If you've ever used C's \texttt{printf()} or Java's \texttt{format()}, then this will be very familiar.  If not, then this might seem strange.

Python makes it moderately easy to output the values of variables in a pleasant-looking way.  With this tool, we can restrict the output size of floating point numbers, or make objects appear in a controllable way.

\subsection{How to Format Output The Old Way}

Formatted output makes use of \textit{formatting strings}, which all start with a percent sign, and \textit{formatting arguments}, which specify the values to be displayed.

Consider this simple example:
\begin{minted}{python}
x = 5.3124806
print( "The value is %d" % (x) )
\end{minted}

The \textit{formatting string} here is \texttt{\%d}, for decimal (base 10) integer, and the \textit{formatting argument} is \texttt{x}.

The general syntax for a formatting string is:

\begin{verbatim}
%[flags][width][.precision]type
\end{verbatim}

Items in square brackets are optional.  Flags differ from type to type.  The width refers to how much horizontal space, in characters, must be reserved to display the value.  Consider this example:

\begin{minted}{python}
print( "The value is %3d" % (x) )
\end{minted}

The output of that example is \texttt{The value is~~~5}.  Note that the value of \texttt{x} was presented in a three-character-wide field.

\subsection{Floating-Point Numbers}

We can present the value of \texttt{x} with whatever precision you want.  The formatting string \mintinline{python}{"%8.2f"} produces the output \texttt{The value is~~~~~5.31}.  Notice that we can leave out the width specifier: \mintinline{python}{print( "The value is %.2f" % (x) )} generates \texttt{The value is 5.31} without any extra spacing.

\subsection{Scientific Notation}

The type \texttt{e} or \texttt{E} can be used to present numbers in scientific notation, with the case of the letter matching the case of the type:

\begin{minted}{python}
avogadro = 602252000000000000000000
print( "Avogadro's Constant is %e" % (avogadro) )
print( "Avogadro's Constant is %E" % (avogadro) )
\end{minted}

\begin{verbatim}
Avogadro's Constant is 6.022520e+23
Avogadro's Constant is 6.022520E+23
\end{verbatim}

The type \texttt{g} or \texttt{G} will choose standard notation or scientific notation as appropriate.

\subsection{Number Bases Besides 10}

Use type \texttt{o} to generate values in octal (base 8), and \texttt{x} or \texttt{X} for hexadecimal (base 16).

\subsection{Multiple Formatting Values}

Formatting strings can appear anywhere within the string to be displayed.  Where $n$ such strings exist, there must be a comma-separated list of $n$ formatting arguments in the parentheses after the percent sign:

\begin{minted}{python}
print( "Do you prefer %E or %d?" % ( avogadro, avogadro ) )
\end{minted}

\subsection{A Note about the \mintinline{python}{format()} Function}

The code above reflects the kind of code used in Lab 2.15, but it's not the most Pythonic way of formatting output.  \href{https://www.python-course.eu/python3_formatted_output.php}{This page} provides a good explanation of the old way and the \mintinline{python}{format} function.

\section{Three Kinds of Python Programs}

There are three different kinds of Python programs.  So far, everything the students have done has been an \textit{interactive} program.  Today's lesson introduces the other two kinds of programs, \textit{scripts} and \textit{modules}.

Both scripts and modules exist in external files named with filenames that end in \texttt{.py}.  As a first approximation of the difference between the two, Python programmers tend to use the word script to refer to what Java programmers think of as the application class, and tend to use the word module when talking about code that has to be imported.  Note that there is no difference in how the files are created or laid out; the only real difference is in how the files are used.

\subsection{Scripts, Modules, and the \mintinline{python}{'__main__'} Function}
\label{sec:main}

We Python programmers are not required to create a main function the way languages like C or Java require us to.  When we ask the Python interpreter to run some code that exists in a \texttt{.py} file -- whether in IDLE, or at the command line, or in some other way -- the interpreter will just start executing whatever code it encounters.  Note that this is also true when a module is \mintinline{python}{import}ed.

Sometimes, however, you will write code that you only want to run when the file is acting as a script -- meaning, when it was invoked directly from the interpreter, rather than imported as a module.  Python sets the special variable \mintinline{python}{__name__} with different values under different conditions.  When a file is being interpreted as a script, \mintinline{python}{__name__} will be set with the value \mintinline{python}{'__main__'}.  Therefore, you can choose to run some code only when it's a script by using this condition:

\begin{minted}{python}
if __name__ == '__main__':
    # do stuff
\end{minted}

I mention it here because zyBooks mentions it here, but I don't intend to explain it in class until Day \ref{day:pythonfunctions}, after we've written some if statements, and when it comes up in a zyLabs assignment.  See Section \ref{sec:main_again} on Page \pageref{sec:main_again} of these notes.

Regarding the world capitals slide, I've created a text file called \texttt{capitals.txt} and uploaded it to the CSC 104 web page; you may want to make this file available to your students, on your I: drive or whatever, so they can copy and paste the capitals instead of having to type them all in.

\section{The Math Module}

Some modules, like \mintinline{python}{math}, come with the language, and just need to be \mintinline{python}{import}ed to be used.  The \mintinline{python}{math} module contains a bunch of useful functions, like \mintinline{python}{math.pow()}.  Notice that when calling a function that's been imported from a module, we must either resolve its scope with the module name and a dot operator, or create a local reference, like \mintinline{python}{pow = math.pow}.  Note that creating a local reference like this is not preferred, because it hides where the \texttt{pow} function comes from, and that can reduce readability.

\section{Representing Text}

This section may be mildly interesting to some people, but I haven't created any slides about it, because I don't expect to talk much about it in class.  Mostly, these things (ASCII/Unicode values, digraphs like \texttt{{\textbackslash}n}) will come up when they come up.  The Participation Activities and Challenge Activities ought to be enough.

\section{Homework}

Assign the Challenge Activites from what was covered today, which is Sections 2.8 - 2.10, and then the Participation Activies from Chapter 3, sections 1 - 5 (which is all there is).