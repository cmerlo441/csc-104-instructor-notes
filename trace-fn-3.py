#! /usr/bin/env python3


def my_function():
    x = 15
    y = 20
    z = (x * 2) + y
    return z


value = my_function()
print('The value is', value)
