#! /usr/bin/env python3


def is_this_ok(x):
    if(1 <= x <= 10):
        return 1
    elif(20 <= x <= 30):
        return 1
    elif(x % 3 == 0):
        return 1
    elif(x > 55):
        return 1
    else:
        return 0


value = int(input('Please enter a number: '))
if(is_this_ok(value) == 1):
    print(value, 'is ok')
else:
    print(value, 'is not ok')
