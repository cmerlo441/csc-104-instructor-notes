#! /usr/bin/env python3

'''Provide a tracing exercise.

What this code does is something of a mystery.  Trace
its results without running it.
'''

sum = 0
exam = int(input('Enter an exam grade: '))
print('You entered', exam)

sum = sum + exam
exam = int(input('Enter another exam grade: '))
print('You entered', exam)

sum = sum + exam
exam = int(input('Enter another exam grade: '))
print('You entered', exam)
sum = sum + exam

average = sum / 3
print('Your exam average is', average)
